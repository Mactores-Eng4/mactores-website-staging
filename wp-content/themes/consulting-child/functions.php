<?php
// $theme_info = wp_get_theme();
// define( 'CONSULTING_THEME_VERSION', ( WP_DEBUG ) ? time() : $theme_info->get( 'Version' ) );


add_action( 'wp_enqueue_scripts', 'consulting_child_enqueue_parent_styles');
function consulting_child_enqueue_parent_styles() {

	wp_enqueue_style( 'consulting-style', get_stylesheet_directory_uri() . '/style.css', array( 'bootstrap' ), CONSULTING_THEME_VERSION, 'all' );

    $skin = get_theme_mod('site_skin', 'skin_default');
    if ($skin == 'skin_default') {
        wp_enqueue_style( 'child-style', get_stylesheet_uri(), array( 'consulting-layout' ), CONSULTING_THEME_VERSION, 'all' );
    } else {
        wp_enqueue_style( 'child-style', get_stylesheet_uri(), array( 'consulting-layout', 'stm-skin-custom-generated' ), CONSULTING_THEME_VERSION, 'all' );
    }

}

add_action( 'wp_enqueue_scripts', 'mactores_load_theme_scripts_and_styles' );
if( ! function_exists( 'mactores_load_theme_scripts_and_styles' ) ){
	function mactores_load_theme_scripts_and_styles() {

		if ( ! is_admin() ) {

			/* Register Scripts */          
            wp_register_script( 'mactores-customjs', get_stylesheet_directory_uri() . '/assets/js/mactores-custom.js', array( 'jquery' ), CONSULTING_THEME_VERSION, true );
            
            /* Enqueue Scripts */
            wp_enqueue_script( 'mactores-customjs' );
		}

	}
}


// WOrdpress hide customization
// customized wp admin top bar
add_action('admin_bar_menu', 'vt_customize_admin_bar_menu', 1001);
function vt_customize_admin_bar_menu( $wp_admin_bar ) {
$wp_admin_bar->remove_node('wp-logo');
$wp_admin_bar->remove_node('td-menu1');
$wp_admin_bar->remove_node('updates');
$wp_admin_bar->remove_node('comments');
$wp_admin_bar->remove_node('new-content');
$wp_admin_bar->remove_node('our_support_item');
}

// removed all notificatiion of themes and plugins.
function remove_core_updates(){
global $wp_version;return(object) array('last_checked'=> time(),'version_checked'=> $wp_version,);
}
add_filter('pre_site_transient_update_core','remove_core_updates');
add_filter('pre_site_transient_update_plugins','remove_core_updates');
add_filter('pre_site_transient_update_themes','remove_core_updates');

// removed bottom wp tag.
function filter_footer_admin() {
	echo '';
}
add_filter('admin_footer_text', 'filter_footer_admin');
function change_footer_version() {return '&nbsp;';}
add_filter( 'update_footer', 'change_footer_version', 9999);

// removed howdy text 
function howdy_message($translated_text, $text, $domain) {
    $new_message = str_replace('Howdy,', '', $text);
    return $new_message;
}
add_filter('gettext', 'howdy_message', 10, 3);
remove_action('wp_head', 'wp_generator');
// removing wordpress version
function wpbeginner_remove_version() {
    return null;
}
add_filter('the_generator', 'wpbeginner_remove_version');

// Remove Windows Live Writer link in header
// Do Not do this if you use it
remove_action('wp_head', 'wlwmanifest_link');
remove_action('wp_head', 'rsd_link');

// remove wp version param from any enqueued scripts
function vc_remove_wp_ver_css_js( $src ) {
    $ver = null;
    if ( strpos( $src, 'ver=' . get_bloginfo( 'version' ) ) )
		$ver = (substr(get_bloginfo( 'version' ), 0, 3) - 3);
        //$src = remove_query_arg( 'ver', $src );
	$src = str_replace("ver=".get_bloginfo( 'version' ), "ver=$ver", $src);
    return $src;
}
add_filter( 'style_loader_src', 'vc_remove_wp_ver_css_js', 9999 );
add_filter( 'script_loader_src', 'vc_remove_wp_ver_css_js', 9999 );

// changed wordpress dashboard title
add_filter('admin_title', 'vt_admin_title', 10, 2);
function vt_admin_title($admin_title, $title)
{
    return get_bloginfo('name').' &bull; '.$title;
}

// removed  admin color scheme from profile page
if ( !current_user_can('manage_options') )
remove_action( 'admin_color_scheme_picker', 'admin_color_scheme_picker' );


// set common theme color for all user
//add_filter('get_user_option_admin_color', 'change_admin_color');
function change_admin_color($result) {
    return 'sunrise';
}

// removed theme page for non manage_options 
function custom_menu_page_removing() {
    if (!current_user_can('manage_options')){
        remove_menu_page( 'themes.php' );
        remove_menu_page( 'vc-welcome' );
    }
    remove_menu_page( 'tools.php' );
//	remove_menu_page( 'plugins.php' );
    remove_menu_page( 'upload.php' );
    remove_menu_page( 'vc-general' );
    remove_menu_page( 'index.php' );
    remove_submenu_page( 'options-general.php', 'options-writing.php' );
//	remove_submenu_page( 'users.php', 'users-user-role-editor.php' );
    //remove_submenu_page( 'themes.php', 'widgets.php' );
    remove_submenu_page( 'themes.php', 'themes.php' );
    global $submenu;
    // Appearance Menu
    unset($submenu['themes.php'][6]);

    // add_menu_page( 'video-job-status.php' );
//	remove_submenu_page( 'index.php', 'update-core.php' );
}

//add_action( 'admin_menu', 'custom_menu_page_removing', 9999 );

//add_filter( 'contextual_help', 'vt_remove_help_tab', 999, 3 );
function vt_remove_help_tab($old_help, $screen_id, $screen){
    $screen->remove_help_tabs();
    return $old_help;
}


function remove_contextual_help() {
    $screen = get_current_screen();
    $screen->remove_help_tabs();
}
add_action( 'admin_head', 'remove_contextual_help' );
# add_filter('screen_options_show_screen', '__return_false');

// hook to hide help toggle in Admin Performance/dashboard
function custom_colors() {
   echo '<style type="text/css">
           #contextual-help-link-wrap{display: none}
         </style>';
}
add_action('admin_head', 'custom_colors');
/*
Disable RSS Fields
*/
function wpb_disable_feed() {
    wp_die( __('No feed available,please visit our <a href="'. get_bloginfo('url') .'">homepage</a>!') );
}

add_action('do_feed', 'wpb_disable_feed', 1);
add_action('do_feed_rdf', 'wpb_disable_feed', 1);
add_action('do_feed_rss', 'wpb_disable_feed', 1);
add_action('do_feed_rss2', 'wpb_disable_feed', 1);
add_action('do_feed_atom', 'wpb_disable_feed', 1);
add_action('do_feed_rss2_comments', 'wpb_disable_feed', 1);
add_action('do_feed_atom_comments', 'wpb_disable_feed', 1);

/*
Remove Forget passoword text
*/

function remove_lostpassword_text ( $text ) {
    if ($text == 'Lost your password?'){$text = '';}
    return $text;
}
add_filter( 'gettext', 'remove_lostpassword_text' );


function _remove_script_version( $src ){

	/* $parts = explode( '?ver', $src );
	//$parts = explode( '?x', $parts[0] );
        return $parts[0];
	*/
        /*if(custom_wp_is_mobile()){
                echo "<script>console.log('mobilee')</script>";
        }else{
                echo "<script>console.log('desktoppp')</script>";
        }*/

	if( strpos( $src, '?ver=' ) ){
 		$src = remove_query_arg( 'ver', $src );
	 }
	 else if (strpos( $src, '?x')) {
 		$src = explode( '?x', $src );
 		$src = $src[0];
	 }
	 return $src;
}
add_filter( 'script_loader_src', '_remove_script_version', 15, 1 );
add_filter( 'style_loader_src', '_remove_script_version', 15, 1 );


function custom_wp_is_mobile($includeTablets=false)
{
   // See if we have the Cloudfront headers
   // See https://aws.amazon.com/blogs/aws/enhanced-cloudfront-customization/ for details.
   // echo "<script>console.log('".$_SERVER['HTTP_CLOUDFRONT_IS_MOBILE_VIEWER']."')</script>";
   // echo "<script>console.log('".wp_is_mobile()."')</script>";
   if (isset($_SERVER['HTTP_CLOUDFRONT_IS_MOBILE_VIEWER'])) {
      if($_SERVER['HTTP_CLOUDFRONT_IS_MOBILE_VIEWER']=='true'){
         return true;
      } elseif($_SERVER['HTTP_CLOUDFRONT_IS_TABLET_VIEWER'] && $includeTablets){
         return true;
      }
      return false; // default
   } else {
      // Fall back to the standard wordpress function for these kind of things.
      return wp_is_mobile();
   }
}
/*
    Prevent the email sending step for specific form
*/
add_action("wpcf7_before_send_mail", "skip_mail_func");  
function skip_mail_func($cf7) {
    // get the contact form object
    $wpcf = WPCF7_ContactForm::get_current();
    /*
    echo "<pre>";
    print_r($wpcf->title);
    echo "</pre>";
    */
    $contact_form_title = "";
    $contact_form_title = $wpcf->title;
    if($contact_form_title=="aws-sap-download-presentation_casestudy" || $contact_form_title=="aws-sap-download-presentation_infographics")
    {
      $wpcf->skip_mail = true;
    }
    return $wpcf;
}

function mac_page_template_redirect()
{
   
   if(isset($_SERVER['REQUEST_URI']) && !empty($_SERVER['REQUEST_URI']) && $_SERVER['REQUEST_URI'] == '/sap-on-aws-webinar/' )
   {
        wp_redirect(site_url()."/sap-on-aws");
   }
}
add_action( 'template_redirect', 'mac_page_template_redirect' );

add_action( 'vc_before_init', 'vc_before_init_actions' );
function vc_before_init_actions() {
    require_once get_stylesheet_directory() . '/gated-asset.php';
}
