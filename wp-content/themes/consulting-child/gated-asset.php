<?php
$cf7 = get_posts( 'post_type="wpcf7_contact_form"&numberposts=-1' );

$contact_forms = array();
if ( $cf7 ) {
    foreach ( $cf7 as $cform ) {
        $contact_forms[ $cform->post_title ] = $cform->ID;
    }
} else {
    $contact_forms[ __( 'No contact forms found', 'js_composer' ) ] = 0;
}

vc_map( array(
        "name" 			=> esc_html__("Gated Asset","xamin"),
        "base" 			=> "xaminGatedAsset",
        "description" 	=> esc_html__("xamin Gated Asset","xamin"),
        "category" 		=> esc_html__( "Xamin Shortcodes","xamin"),
        "icon" 			=> "icon-wpb-xamintitle",
        "params" => array(

            array(
                "type" => "vc_link",
                "heading" => __( "URL (Link)", "xamin" ),
                "param_name" => "link",
                "description" => __( "Add custom link.", "xamin" ),
            ),

            array(
                "type" 			=> "textfield",
                "heading" 		=> esc_html__("Case Study Title", "xamin" ),
                "param_name" 	=> "case_study_title",
                "description" 	=> esc_html__("Add Case Study Title.", "xamin" ),
            ),

//            array(
//                'type' => 'dropdown',
//                'heading' => __( 'Select contact form', 'xamin' ),
//                'param_name' => 'contact_form_id',
//                'value' => $contact_forms,
//                'save_always' => true,
//                'description' => __( 'Choose previously created contact form from the drop down list.', 'xamin' ),
//            ),

//            array(
//                "type" => "dropdown",
//                "class" => "",
//                "heading" =>  esc_html__("Button Position","xamin"),
//                "param_name" => "button_position",
//                "edit_field_class" => "vc_col-xs-6",
//                "value" => array(
//                    "Left" =>  "left",
//                    "Center" => "center",
//                    "Right" => "right",
//                ),
//
//            ),

//            array(
//                "heading"       => esc_html__( "Button White","xamin" ),
//                "description"   => esc_html__( "button white.","xamin"),
//                "type"          => "dropdown",
//                "param_name"    => "button_white",
//                "edit_field_class" => "vc_col-xs-6",
//                "value"         => array(
//                    "Default"    => "1",
//                    "Button-White" => "2",
//                    "Button-Blue" => "3",
//                    "Button-Border" => "4",
//                ),
//            ),

//            array(
//                "heading"		=> esc_html__( "Title Color","xamin" ),
//                "type"			=> "colorpicker",
//                "group" => esc_html__( "Color", "xamin" ),
//                "edit_field_class" => "vc_col-xs-6",
//                "param_name"	=> "title_color",
//                "value"      => "#525f81",
//            ),

//            array(
//                "heading"		=> esc_html__( "background Color","xamin" ),
//                "type"			=> "colorpicker",
//                "group" => esc_html__( "Color", "xamin" ),
//                "edit_field_class" => "vc_col-xs-6",
//                "param_name"	=> "background_color",
//                "value"      => "#525f81",
//            ),

            array(
                "type"        => "textfield",
                "heading"     => esc_html__( "Extra class name","xamin" ),
                "param_name"  => "extra_class",
                "description" => esc_html__( "If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.","xamin" ),
                "group" => esc_html__( "Extra class", "xamin" ),
            ),

            array(
                "type" 		=> "css_editor",
                "heading" 	=> esc_html__( "Css", "xamin" ),
                "param_name" => "css",
                "group" => esc_html__( "Design options", "xamin" ),
            ),


        ),
    )
);

function xaminGatedAsset( $atts, $content = null ) {

    $data = wp_parse_args( $atts, array(
        'link'                    => '',
        'button_position'        => 'left',
        'button_white'		=> '1',
        'extra_class'        => '',
        'css' => '',
        'title_color'		=> '',
        'background_color'		=> '',
        'case_study_title' => '',
    ) );

    $css_class = '';

    $link = $data['link'] ? $data["link"] : '';
    $case_study_title = $data['case_study_title'] ? $data["case_study_title"] : '';

    $button_white = $data['button_white'] ? $data["button_white"] : '';

    $extra_class = $data['extra_class'] ? $data["extra_class"] : '';

    $button_position = $data['button_position'] ? $data["button_position"] : '';

    $css = $data['css'] ? $data["css"] : '';

    $title_color = $data['title_color'] ? $data["title_color"] : '';

    $background_color = $data['background_color'] ? $data["background_color"] : '';

    $title_color = $title_color ? ' style="color: ' . $title_color . ';"' : '';
    $background_color = $background_color ? ' style="background: ' . $background_color . ';"' : '';

    if(!empty($css)){
        $css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $css, ' ' ), $atts['css'] );
    }


    $link = ('||'===$link)?'':$link;
    $link = vc_build_link($link);
    $a_href =$link['url'];
    $a_title = $link['title'];


    $out ='';

    $out .='<div class=" text-'.$button_position.' '.$extra_class.' '. esc_attr( $css_class ) .'">';
    $out .='<a class="vc_general vc_btn3 vc_btn3-size-md vc_btn3-shape-rounded vc_btn3-style-flat vc_btn3-color-success gated-asset-button" '.$background_color.' href="'. $a_href . '" title="'.$case_study_title.'"><span class="btn-effect" '.$title_color.'> '.$a_title.'</span></a>';
//    if($button_white == 1)
//    {
//        $out .='<a class="button button-icon gated-asset-button" '.$background_color.' href="'. $a_href . '" title="'.$case_study_title.'"><span class="btn-effect" '.$title_color.'> '.$a_title.'</span></a>';
//    }
//    if($button_white == 2)
//    {
//        $out .='<a class="button white-btn button-icon gated-asset-button" href="'. $a_href . '" title="'.$case_study_title.'"><span class="btn-effect"> '.$a_title.'</span></a>';
//    }
//    if($button_white == 3)
//    {
//        $out .='<a class="button blue-btn button-icon gated-asset-button" href="'. $a_href . '" title="'.$case_study_title.'"><span class="btn-effect"> '.$a_title.'</span></a>';
//    }
//    if($button_white == 4)
//    {
//        $out .='<a class="button border-btn button-icon gated-asset-button" href="'. $a_href . '" title="'.$case_study_title.'"><span class="btn-effect"> '.$a_title.'</span></a>';
//    }
    $out .='</div>';
    return $out;


}
add_shortcode('xaminGatedAsset', 'xaminGatedAsset');

add_action( 'wp_footer', 'gated_asset_modal' );

function gated_asset_modal() {
    ?>
    <div id="gated-asset-modal" class="modal fade" role="dialog">
        <div class="modal-dialog modal-md">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
			 <h5 class="modal-title">Submit Information</h5>

                </div>
                <div class="modal-body">
                    <?php echo do_shortcode( '[contact-form-7 id="4526" title="Download Case Study Form"]' ); ?>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        jQuery('.gated-asset-button').on('click', function(e) {
            e.preventDefault();
            jQuery("#downloadPresentation").attr('href', jQuery(this).attr('href'));
            jQuery("input[id='case-study-title']").val(jQuery(this).attr('title'));
            jQuery("#gated-asset-modal").modal("show");
        });
        document.addEventListener( 'wpcf7mailsent', function( event ) {
            if ( '4526' == event.detail.contactFormId ) {
                document.getElementById('downloadPresentation').click();
                jQuery("#gated-asset-modal").modal("hide");
            }
        }, false );
    </script>
    <?php
}
?>
