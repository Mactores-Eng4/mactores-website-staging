jQuery(document).ready(function($) {
    "use strict";

    /* scroll to top js */
    var offset = 300,
        //browser window scroll (in pixels) after which the "back to top" link opacity is reduced
        offset_opacity = 1200,
        //duration of the top scrolling animation (in ms)
        scroll_top_duration = 700,
        //grab the "back to top" link
        $back_to_top = $('.cd-top');

    //hide or show the "back to top" link
    $(window).scroll(function() {
        ($(this).scrollTop() > offset) ? $back_to_top.addClass('cd-is-visible'): $back_to_top.removeClass('cd-is-visible cd-fade-out');
        if ($(this).scrollTop() > offset_opacity) {
            $back_to_top.addClass('cd-fade-out');
        }
    });

    //smooth scroll to top
    $back_to_top.on('click', function(event) {
        event.preventDefault();
        $('body,html').animate({
            scrollTop: 0,
        }, scroll_top_duration);
    });

    /* contact us form pop up on home page */
    $('.open-contact-form').on('click', function(e) {
        $("#contact-us-modal").modal("toggle");
    });

    /* contact us form pop up to download presentation */
    $('.contact-form-popup-download-file-presentation').on('click', function(e) {
        $("#contact-us-modal-download-presentation").modal("toggle");
    });

    /* apply for designnation form on career */
    $('.apply-to-post').on('click', function(e) {
        var classnames = this.className;
        getAppliedDesign(classnames);
    });

    /* add download attribute to button */
    if ($(".download-pdf").length) {
        $('.download-pdf a').attr('download', 'pdffile');
    }

    /* SAP AWS contact us form pop up to download presentation */
    $('.aws-sap-download-presentaion').on('click', function(e) {
        $("#aws-sap-download-presentaion-cs-modal").modal("toggle");
    });

    function getAppliedDesign(classNames) {
        $("#applyforjob-modal").modal("toggle");
        var post_applied_for = $.grep(classNames.split(" "), function(v, i) {
            return v.indexOf('job') === 0;
        }).join();
        if (post_applied_for) {
            var job = post_applied_for.split('-')[1].replace(/_/g, " ");
            $("#applicant_design").val(job).attr('readonly', 'true').css('text-transform', 'capitalize');
        }
    }

});
