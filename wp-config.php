<?php
/*
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', '');

/** MySQL database username */
define('DB_USER', '');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', '');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');


/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '%F:*^+4CKSs6V@+6`C[HGjxVhw*0nc3d}{ha*}wN=O{]Xqv+CJ*Z_FL`x%>1`RJ)');
define('SECURE_AUTH_KEY',  'j@D^LLu%c>RI^aSXmQ?vRuRrTbpe.:Ix;)HH-bUl|*mM6%;E|/,wl%e npmanNAY');
define('LOGGED_IN_KEY',    'Puo?9oXP(/=+nCZz(WRZ*|fHOuPh5HgNK6~X&-|_U>w]L>=WtvW4Jq/_oz84co6g');
define('NONCE_KEY',        '.c3+Q-g0z5wYE3CGMLe*&lZQ]D^W;Ky<~Uy{Cdt+6-Wk>`#?loYS%GQpH|A:Z9Y`');
define('AUTH_SALT',        ':P,7#5:uu*Z8}*(_>AlyQ+bM^GT!pCXE5z)<i{$H5g+aYBg6rup=})|DeL/NPyGb');
define('SECURE_AUTH_SALT', 'AQKt}%G14r:^>B9TT+=Fd!#,]@fS>#@;}DznX*d+K4G!NFz.Y(2NXkfZs?XG}U&I');
define('LOGGED_IN_SALT',   '|>};hc?+ZiKH|G}1a=`&,ZmT,(;:/oGswk69UJmuyyKU)aOCfqs+-58/6MD09K>b');
define('NONCE_SALT',       'ZeNY V,6{(|!g?&l-R{yE=)}{+*&3. kUWRKE|^7-}4m93#^nr*KJrO=3DYOIa}@');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'kfufy_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);
// Enable Debug logging to the /wp-content/debug.log file
define( 'WP_DEBUG_LOG', false);
// Disable display of errors and warnings 
define( 'WP_DEBUG_DISPLAY', false );
@ini_set( 'display_errors', false );

/*
 * Handle multi domain into single instance of wordpress installation
 */
/*if (!empty($_SERVER['HTTPS']) && ('on' == $_SERVER['HTTPS'])) {
    $URI = 'https://';
}
else if (!empty($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https'){
    // If SSL certificate was install on load balancer/application gateway
    // Need to enable HTTPS
    $_SERVER['HTTPS'] = 'on';
    $URI = 'https://';
}
else {
    $URI = 'http://';
}
$DOMAIN_NAME = 'www.mactores.com';
$SITE_URL = $URI.$DOMAIN_NAME;
define('WP_HOME', $SITE_URL);
define('WP_SITEURL',$SITE_URL);

define( 'WP_CONTENT_DIR', dirname(__FILE__) .'/custom' );
define( 'WP_CONTENT_URL', WP_SITEURL.'/custom' );
define( 'WP_PLUGIN_DIR', WP_CONTENT_DIR.'/mod' );
define( 'WP_PLUGIN_URL', WP_SITEURL.'/custom/mod' );

//Disable File Edits
define('DISALLOW_FILE_EDIT', true);*/

# error_reporting(0);

#define('FORCE_SSL_ADMIN', true);
#define('FORCE_SSL_LOGIN', true);i

# define( 'WC_REMOVE_ALL_DATA', true);  // uncomment it to delete woocommerce db table before deleting plugin

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
?>
